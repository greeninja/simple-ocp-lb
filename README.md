# Simple Nginx Load Balancer

All configuration is held in `files/nginx.conf` which is copied into the container during build.

Currently this only has configured the Control plane nodes, and the ingress for http(s) will need to be filled out.

## To build the image:

```bash
buildah bud -t ocp4-nginx-lb
```

## To configure the image

Change `files/nginx.conf` to update the upstreams or configure the ingress http upsteams.

## To run the container

To run the container in the foreground:

```bash
podman run -p 80:80 -p 443:443 -p 6443:6443 -p 22623:22623 localhost/ocp4-nginx-lb:latest
```

## Note

This is __NOT__ for production, and is just a simple proxy for poc work when nothing better is available
