# Based on UBI Nginx
FROM ubi8/nginx-118

COPY files/nginx.conf /etc/nginx/nginx.conf

EXPOSE ["80", "443", "6443", "22623"]

CMD nginx -g "daemon off;"

